//package com.example.demo;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Arrays;
//
//@Service
//public class UserService implements IUserService {
//    @Autowired
//    private UserRepository repository;
//
//    @Transactional
//    @Override
//    public User registerNewUserAccount(UserDto accountDto) throws Exception {
//        if (emailExist(accountDto.getEmail())) {
//            throw new Exception ("There is an account with that email address:"  + accountDto.getEmail());}
//        User user = new User();
//        user.setFirstName(accountDto.getFirstName());
//        user.setLastName(accountDto.getLastName());
//        user.setPassword(accountDto.getPassword());
//        user.setEmail(accountDto.getEmail());
//        user.setRoles(Arrays.asList("USER"));
//        return repository.save(user);
//    }
//    private boolean emailExist(String email) {
//        User user = repository.findByEmail(email);
//        if (user != null) {
//            return true;
//        }
//        return false;
//    }
//}