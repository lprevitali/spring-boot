package com.example.demo;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class ApplicationController {


    private RepositoryPersone repository;
//    private UserRepository userRepository;

    @Autowired
    public ApplicationController(RepositoryPersone repository) {
        this.repository = repository;
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String leggiPersona(Model model,  @RequestParam(value = "nomedacercare", required=false) String nome) {
        if(nome == null){
            List<Persona> persone = repository.findAll();
            if (persone != null) {
                model.addAttribute("persone", persone);
            }
            return "tantePersone";
        }
        else{
            List<Persona> persone = repository.findByNome(nome);
            if (persone != null) {
                model.addAttribute("persone", persone);
            }
            return "tantePersone";
        }

    }

    @RequestMapping(value="/", method=RequestMethod.POST)
    public String aggiungiPersona(Persona persona) {
        Persona cf = repository.findOneByCf(persona.getCf());
        if(cf == null) {
            repository.save(persona);
            return "redirect:/";
        }
        else return "already";
    }

    @RequestMapping(value="/details", method=RequestMethod.GET)
    public String dettaglioUtenti(Model model, @RequestParam("cf") String cf){
        Persona persona = repository.findOneByCf(cf);
        model.addAttribute("persona", persona);
        return "details";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "redirect:/";
    }

}
