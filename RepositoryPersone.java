package com.example.demo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryPersone extends JpaRepository<Persona, String> {
    List<Persona> findByNome(String nome);
    Persona findOneByCf(String cf);
//    Persona findOneByNomeAndCognome(String nome, String cognome);
}
