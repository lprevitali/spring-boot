package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prova")
public class restController {
    @RequestMapping(method = RequestMethod.GET)
    public String curlProva(@RequestParam(value = "frase") String test) {
        String reverse = new StringBuffer(test).reverse().toString();
        return reverse;
    }
}
